import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ManufacturersComponent} from './modules/admin/manufactirers/components/manufacturers/manufacturers.component';
import {EquipmentTypesComponent} from './modules/admin/equipment-types/components/equipment-types/equipment-types.component';
import {RepairsComponent} from './modules/admin/repairs/components/repairs/repairs.component';
import {ModelsComponent} from './modules/admin/models/components/models/models.component';
import {ModelsModule} from './modules/admin/models/models.module';
import {UsersComponent} from './modules/admin/users/components/users/users.component';
import {EquipmentsComponent} from './modules/admin/equipment/components/equipments/equipments.component';
import {UsersModule} from './modules/admin/users/users.module';
import {EquipmentModule} from './modules/admin/equipment/equipment.module';
import {RepairsModule} from './modules/admin/repairs/repairs.module';
import {ManufactirersModule} from './modules/admin/manufactirers/manufactirers.module';
import {EquipmentTypesModule} from './modules/admin/equipment-types/equipment-types.module';
import {RentComponent} from './modules/user/rent/rent.component';
import {UserMainPageComponent} from './modules/user/user-main-page/user-main-page.component';
import {UserRepairComponent} from './modules/user/user-repair/user-repair.component';
import {NewTicketsComponent} from './modules/repair-engineer/new-tickets/new-tickets.component';
import {InWorkTicketsComponent} from './modules/repair-engineer/in-work-tickets/in-work-tickets.component';
import {ClosedTicketsComponent} from './modules/repair-engineer/closed-tickets/closed-tickets.component';

const routes: Routes = [
  {
    path: 'users',
    component: UsersComponent
  },
  {
    path: 'equipments',
    component: EquipmentsComponent
  },
  {
    path: 'repairs',
    component: RepairsComponent
  },
  {
    path: 'models',
    component: ModelsComponent
  },
  {
    path: 'manufacturers',
    component: ManufacturersComponent
  },
  {
    path: 'equipment-types',
    component: EquipmentTypesComponent
  },
  {
    path: 'user-rent',
    component: RentComponent
  },
  {
    path: 'user-main-page',
    component: UserMainPageComponent
  },
  {
    path: 'user-repair',
    component: UserRepairComponent
  },
  {
    path: 'engineer-new-tickets',
    component: NewTicketsComponent
  },
  {
    path: 'engineer-in-work-tickets',
    component: InWorkTicketsComponent
  },  {
    path: 'engineer-closed-tickets',
    component: ClosedTicketsComponent
  },

  // { path: '**', component: UsersComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    UsersModule,
    EquipmentModule,
    RepairsModule,
    ModelsModule,
    ManufactirersModule,
    EquipmentTypesModule,
    ModelsModule
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
