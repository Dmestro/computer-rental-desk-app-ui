import { Component, OnInit } from '@angular/core';
import {Repair} from "../../user/user-repair/model";
import {EquipmentsService} from "../../admin/equipment/equipments.service";
import {RentService} from "../../user/rent/rent.service";
import {UsersService} from "../../admin/users/users.service";
import {AuthService} from "../../auth/auth.service";
import {RepairService} from "../../user/user-repair/repair.service";
import {combineLatest} from "rxjs";

@Component({
  selector: 'crd-closed-tickets',
  templateUrl: './closed-tickets.component.html',
  styleUrls: ['./closed-tickets.component.less']
})
export class ClosedTicketsComponent implements OnInit {

  public repairs: Repair[];
  private selectedRepair: Repair;
  repairStatuses: string[] = ["Принят в ремонт", "В ремонте", "Ремонт завершен"];
  repairTypes: string[] = ["Гарантийный", "Оплачиваемый"];

  constructor(
    public equipmentsService: EquipmentsService,
    public rentService: RentService,
    public usersService: UsersService,
    public authService: AuthService,
    public repairsService: RepairService,
  ) { }

  ngOnInit(): void {
    this.repairsService.getAll().subscribe(data => {
      combineLatest(
        this.equipmentsService.getAll(),
        this.usersService.getAll(),
        this.repairsService.getAll()
      ).subscribe(([equipments, users, repairs]) => {
        if (equipments && users && repairs) {
          this.updateData(equipments, users, repairs);
        }
      });


    })
  }

  setSelectedItem(et: Repair) {
    this.selectedRepair = et;
  }

  public updateData(equipments, users, repairs: Repair[]): void {
    this.repairs = repairs.filter(r => r.status === "Ремонт завершен"
      && r.serviceEngineer &&
      r.serviceEngineer.id === this.authService.getUserParams().id);

    this.repairs.forEach(r => {
      r.equipment = equipments.find(e => e.id === r.equipment.id);
    });
  }

  save() {
    this.repairsService.save(this.selectedRepair).subscribe(data => {
      combineLatest(
        this.equipmentsService.getAll(),
        this.usersService.getAll(),
        this.repairsService.getAll()
      ).subscribe(([equipments, users, repairs]) => {
        if (equipments && users && repairs) {
          this.updateData(equipments, users, repairs);
        }
      });
    });
  }

}
