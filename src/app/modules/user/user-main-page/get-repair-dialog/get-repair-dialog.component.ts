import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {UsersService} from '../../../admin/users/users.service';
import {AuthService} from '../../../auth/auth.service';
import {RepairService} from '../../user-repair/repair.service';
import {Repair} from '../../user-repair/model';

class DialogData {
}

@Component({
  selector: 'crd-get-repair-dialog',
  templateUrl: './get-repair-dialog.component.html',
  styleUrls: ['./get-repair-dialog.component.less']
})
export class GetRepairDialogComponent implements OnInit {

  public comment = '';
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private usersService: UsersService,
    private authSerivce: AuthService,
    private repairsService: RepairService
    ) { }

  ngOnInit(): void {
    this.usersService.getAll();
  }

  sendRepairReport(): void {
      const user  = this.usersService.users.find(u => u.id === this.authSerivce.getUserParams().id);
      const equipment = (this.data as any).rent.equipment;

      const repair = new Repair();
      repair.equipment = equipment;
      repair.user = user;
      repair.comment = this.comment;
      repair.status = 'new';

      this.repairsService.create(repair);
  }
}
