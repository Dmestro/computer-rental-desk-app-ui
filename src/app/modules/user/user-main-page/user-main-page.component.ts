import { Component, OnInit } from '@angular/core';
import {EquipmentsService} from '../../admin/equipment/equipments.service';
import {RentService} from '../rent/rent.service';
import {UsersService} from '../../admin/users/users.service';
import {AuthService} from '../../auth/auth.service';
import {combineLatest} from 'rxjs';
import {Rent} from '../rent/model';
import {htmlAstToRender3Ast} from '@angular/compiler/src/render3/r3_template_transform';
import {MatDialog} from '@angular/material/dialog';
import {CreateModelDialogComponent} from '../../admin/models/components/create-model-dialog/create-model-dialog.component';
import {GetRepairDialogComponent} from './get-repair-dialog/get-repair-dialog.component';
import {RepairService} from '../user-repair/repair.service';
import {Repair} from '../user-repair/model';

@Component({
  selector: 'crd-user-main-page',
  templateUrl: './user-main-page.component.html',
  styleUrls: ['./user-main-page.component.less']
})
export class UserMainPageComponent implements OnInit {

  public rents: Rent[] = [];
  public repairs: Repair[] = [];
  public selectedRent: Rent;
  private selectedRepair: Repair;


  constructor(
    public equipmentsService: EquipmentsService,
    public rentService: RentService,
    public usersService: UsersService,
    public authService: AuthService,
    public repairsService: RepairService,
    private dialog: MatDialog
  ) {
  }

  ngOnInit(): void {
    combineLatest(
      this.equipmentsService.getAll(),
      this.rentService.getAll(),
      this.usersService.getAll(),
      this.repairsService.getAll()
    ).subscribe(([equipments, rents, users, repairs]) => {
      if (equipments && rents && users && repairs) {
        this.updateData(equipments, rents, users, repairs);
      }
    });

  }

  public updateData(equipments, rents, users, repairs: Repair[]): void {
    this.rents = rents.filter(r => {
      return r.user.id === this.authService.getUserParams().id;
    });

    this.rents.forEach(r => {
      r.equipment = equipments.find(e => e.id === r.equipment.id);
    });

    this.repairs = repairs.filter(r => r.user.id === this.authService.getUserParams().id && r.status !== "Ремонт завершен");

    this.repairs.forEach(r => {
      r.equipment = equipments.find(e => e.id === r.equipment.id);
      this.rents = this.rents.filter(re => re.equipment.id !== r.equipment.id);
    });

    console.log(this.rents, this.repairs);
  }

  setSelectedItem(et: Rent): void {
    this.selectedRent = et;
  }

  setSelectedRepair(r: Repair): void {
    this.selectedRepair = r;
  }

  public getRepair(): void {
    const dialogRef = this.dialog.open(GetRepairDialogComponent, {
      data: {
        rent: this.selectedRent
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog repair result: ${result}`);
      combineLatest(
        this.equipmentsService.getAll(),
        this.rentService.getAll(),
        this.usersService.getAll(),
        this.repairsService.getAll()
      ).subscribe(([equipments, rents, users, repairs]) => {
        if (equipments && rents && users && repairs) {
          this.updateData(equipments, rents, users, repairs);
        }
      });
    });
  }

  declineRent(): void {
    console.log(this.selectedRent);
    this.rentService.delete(this.selectedRent);
    this.rents = this.rents.filter(r => r.id !== this.selectedRent.id);
    this.selectedRent = null;
  }

  declineRepair(): void {
    this.repairsService.delete(this.selectedRepair).subscribe(() => {
      combineLatest(
        this.equipmentsService.getAll(),
        this.rentService.getAll(),
        this.usersService.getAll(),
        this.repairsService.getAll()
      ).subscribe(([equipments, rents, users, repairs]) => {
        if (equipments && rents && users && repairs) {
          this.updateData(equipments, rents, users, repairs);
        }
      });
    })
  }
}
