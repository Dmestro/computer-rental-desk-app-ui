import { Component, OnInit } from '@angular/core';
import {EquipmentsService} from '../../admin/equipment/equipments.service';
import {Equipment} from '../../admin/equipment/model';
import {RentService} from './rent.service';
import {Rent} from './model';
import {UsersService} from '../../admin/users/users.service';
import {AuthService} from '../../auth/auth.service';
import {combineAll, filter} from 'rxjs/operators';
import {combineLatest} from 'rxjs';

@Component({
  selector: 'crd-rent',
  templateUrl: './rent.component.html',
  styleUrls: ['./rent.component.less']
})
export class RentComponent implements OnInit {

  public selectedEquipment: Equipment;
  public rentTimes  = ['1 месяц', '2 месяца', '6 месяцев', '1 год'];
  public selectedRentTime = this.rentTimes[0];

  public equipments: Equipment[] = [];


  constructor(
    public equipmentsService: EquipmentsService,
    public rentService: RentService,
    public usersService: UsersService,
    public authService: AuthService
  ) { }

  ngOnInit(): void {

    combineLatest(
      this.equipmentsService.getAll(),
      this.rentService.getAll(),
      this.usersService.getAll()
    ).subscribe(([equipments, rents , users]) => {
      if (equipments && rents && users){
        console.log(equipments, rents, users);

        this.equipments = equipments.filter(e => {
          const isInRent = rents.some(r => r.equipment.id === e.id);

          return !isInRent;
        });
      }

      }
    );

  }

  setSelectedItem(item: Equipment): void {
    this.selectedEquipment = JSON.parse(JSON.stringify(item));
    this.selectedRentTime = this.rentTimes[0];
  }

  getRent(): void {
    const rent = new Rent();
    rent.equipment = this.selectedEquipment;
    rent.rentTime = this.selectedRentTime;
    rent.startRentDate = new Date();
    const currentUserId = this.authService.getUserParams().id;
    const user = this.usersService.users.find(u => u.id === currentUserId);
    rent.user = user;

    this.rentService.create(rent).subscribe(data => {
      combineLatest(
        this.equipmentsService.getAll(),
        this.rentService.getAll(),
        this.usersService.getAll()
      ).subscribe(([equipments, rents , users]) => {
          if (equipments && rents && users){
            console.log(equipments, rents, users);

            this.equipments = equipments.filter(e => {
              const isInRent = rents.some(r => r.equipment.id === e.id);

              return !isInRent && e.availability;
            });
          }

        }
      );
      this.selectedRentTime = this.rentTimes[0];
    });
  }
}
