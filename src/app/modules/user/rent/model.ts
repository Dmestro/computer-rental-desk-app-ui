import {User} from '../../admin/users/model';
import {Equipment} from '../../admin/equipment/model';

export class Rent {
  id?: string;
  user?: User;
  equipment: Equipment;
  rentTime: string;
  startRentDate: Date;
}
