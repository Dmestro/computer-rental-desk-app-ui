import { Injectable } from '@angular/core';
import {Rent} from './model';
import {HttpClient} from '@angular/common/http';
import {Observable, Subject} from 'rxjs';
import {BACKEND_HOST} from '../../../constants';
import {map, take} from 'rxjs/operators';
import {Equipment} from '../../admin/equipment/model';

@Injectable({
  providedIn: 'root'
})
export class RentService {

  private endpoint = '/rent';
  rents: Rent[];

  constructor(private http: HttpClient) { }

  public getAll(): Observable<Rent[]> {
    const res$ = this.http.get(BACKEND_HOST + this.endpoint);

    const s$ = new Subject<Rent[]>();

    res$.pipe(take(1)).subscribe((data: Rent[]) => {
      this.rents = data;
      s$.next(data);
    });
    return s$.asObservable();
  }

  public save(equipment: Equipment): Observable<Rent> {
    const res$ = this.http.put(BACKEND_HOST + this.endpoint + '/' + equipment.id, equipment);

    res$.pipe(take(1)).subscribe(
      (et: Rent) => {
        const index: number = this.rents.findIndex(i => i.id === et.id);
        if (index === -1) {
          this.rents.push(et);
        } else {
          this.rents[index] = et;
        }
      }
    );

    return res$.pipe(map(r => r as Rent));

  }

  delete(equipment: Rent): Observable<any> {
    const res$ = this.http.delete(BACKEND_HOST + this.endpoint + '/' + equipment.id);

    res$.pipe(take(1)).subscribe(
      () => {
        this.rents = this.rents.filter((et: Rent) => et.id !== equipment.id);
      }
    );

    return res$;
  }

  create(equipment: Rent): Observable<Rent> {
    const res$ = this.http.post(BACKEND_HOST + this.endpoint, equipment);

    const s$ = new Subject<Rent>();

    res$.subscribe((et: Rent) => {
      this.rents.push(et);
      s$.next(et);
    });
    return s$.asObservable();
  }
}
