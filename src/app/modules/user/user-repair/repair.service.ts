import { Injectable } from '@angular/core';
import {Rent} from '../rent/model';
import {HttpClient} from '@angular/common/http';
import {Observable, Subject} from 'rxjs';
import {BACKEND_HOST} from '../../../constants';
import {map, take} from 'rxjs/operators';
import {Equipment} from '../../admin/equipment/model';
import {Repair} from './model';

@Injectable({
  providedIn: 'root'
})
export class RepairService {

  private endpoint = '/repair';
  repairs: Repair[];

  constructor(private http: HttpClient) { }

  public getAll(): Observable<Repair[]> {
    const res$ = this.http.get(BACKEND_HOST + this.endpoint);

    const s$ = new Subject<Repair[]>();

    res$.pipe(take(1)).subscribe((data: Repair[]) => {
      this.repairs = data;
      s$.next(data);
    });
    return s$.asObservable();
  }

  public save(equipment: Repair): Observable<Repair> {
    const res$ = this.http.put(BACKEND_HOST + this.endpoint + '/' + equipment.id, equipment);

    res$.pipe(take(1)).subscribe(
      (et: Repair) => {
        const index: number = this.repairs.findIndex(i => i.id === et.id);
        if (index === -1) {
          this.repairs.push(et);
        } else {
          this.repairs[index] = et;
        }
      }
    );

    return res$.pipe(map(r => r as Repair));

  }

  delete(equipment: Repair): Observable<any> {
    const res$ = this.http.delete(BACKEND_HOST + this.endpoint + '/' + equipment.id);

    res$.pipe(take(1)).subscribe(
      () => {
        this.repairs = this.repairs.filter((et: Repair) => et.id !== equipment.id);
      }
    );

    return res$;
  }

  create(equipment: Repair): Observable<Repair> {
    const res$ = this.http.post(BACKEND_HOST + this.endpoint, equipment);

    const s$ = new Subject<Repair>();

    res$.subscribe((et: Repair) => {
      this.repairs.push(et);
      s$.next(et);
    });
    return s$.asObservable();
  }
}
