import {Equipment} from '../../admin/equipment/model';
import {User} from '../../admin/users/model';

export class Repair {
  id?: string;
  user: User;
  serviceEngineer?: User;
  equipment: Equipment;
  price: number;
  comment: string;
  status: string;

  type: string;
  materials: string;

  endDate: Date
}
