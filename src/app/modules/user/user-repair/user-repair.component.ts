import { Component, OnInit } from '@angular/core';
import {Repair} from "./model";
import {EquipmentsService} from "../../admin/equipment/equipments.service";
import {RentService} from "../rent/rent.service";
import {UsersService} from "../../admin/users/users.service";
import {AuthService} from "../../auth/auth.service";
import {RepairService} from "./repair.service";
import {combineLatest} from "rxjs";

@Component({
  selector: 'crd-user-repair',
  templateUrl: './user-repair.component.html',
  styleUrls: ['./user-repair.component.less']
})
export class UserRepairComponent implements OnInit {

  public repairs: Repair[];
  private selectedRepair: Repair;
  repairStatuses: string[] = ["Принят в ремонт", "В ремонте", "Ремонт завершен"];
  repairTypes: string[] = ["Гарантийный", "Оплачиваемый"];

  constructor(
    public equipmentsService: EquipmentsService,
    public rentService: RentService,
    public usersService: UsersService,
    public authService: AuthService,
    public repairsService: RepairService,
  ) { }

  ngOnInit(): void {
    this.repairsService.getAll().subscribe(data => {
      combineLatest(
        this.equipmentsService.getAll(),
        this.usersService.getAll(),
        this.repairsService.getAll()
      ).subscribe(([equipments, users, repairs]) => {
        if (equipments && users && repairs) {
          this.updateData(equipments, users, repairs);
        }
      });


    })
  }

  setSelectedItem(et: Repair) {
    this.selectedRepair = et;
  }

  public updateData(equipments, users, repairs: Repair[]): void {
    this.repairs = repairs.filter(r => r.user.id === this.authService.getUserParams().id);

    this.repairs.forEach(r => {
      r.equipment = equipments.find(e => e.id === r.equipment.id);
    });
  }

  save() {
    this.repairsService.save(this.selectedRepair).subscribe(data => {
      combineLatest(
        this.equipmentsService.getAll(),
        this.usersService.getAll(),
        this.repairsService.getAll()
      ).subscribe(([equipments, users, repairs]) => {
        if (equipments && users && repairs) {
          this.updateData(equipments, users, repairs);
        }
      });
    });
  }

}
