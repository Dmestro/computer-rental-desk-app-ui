import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {BACKEND_HOST} from '../../../constants';
import {Manufacturer} from './model';

@Injectable({
  providedIn: 'root'
})
export class ManufacturersService {

  constructor(private http: HttpClient) { }

  public getAll(): Observable<any> {
    return this.http.get(BACKEND_HOST + '/manufacturers');
  }

  public save(manufacturer: Manufacturer): Observable<any> {
    return this.http.put(BACKEND_HOST + '/manufacturers/' + manufacturer.id, manufacturer);
  }

  delete(manufacturer: Manufacturer): Observable<any> {
    return this.http.delete(BACKEND_HOST + '/manufacturers/' + manufacturer.id);
  }

  create(manufacturer: Manufacturer): Observable<any> {
    return this.http.post(BACKEND_HOST + '/manufacturers', manufacturer);
  }
}
