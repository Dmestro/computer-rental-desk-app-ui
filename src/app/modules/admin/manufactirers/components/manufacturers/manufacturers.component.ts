import { Component, OnInit } from '@angular/core';
import {ManufacturersService} from '../../manufacturers.service';
import {Manufacturer} from '../../model';
import {MatDialog} from '@angular/material/dialog';
import {CreateManufacturerDialogComponent} from '../create-manufacturer-dialog/create-manufacturer-dialog.component';

@Component({
  selector: 'crd-manufacturers',
  templateUrl: './manufacturers.component.html',
  styleUrls: ['./manufacturers.component.less']
})
export class ManufacturersComponent implements OnInit {

  public manufacturers: Manufacturer[] = [];
  private selectedManufacturer: Manufacturer;

  constructor(
    private manufacturersService: ManufacturersService,
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.manufacturersService.getAll().subscribe(data => {
      this.manufacturers = data;
    });
  }

  setSelectedManufacturer(manufacturer: any): void {
    this.selectedManufacturer = JSON.parse(JSON.stringify(manufacturer));
  }

  onCancelEdit(): void {
    this.selectedManufacturer = JSON.parse(JSON.stringify(
      this.manufacturers.find(m => m.id === this.selectedManufacturer.id)
    ));
  }

  onSave(): void {
    this.manufacturersService.save(this.selectedManufacturer).subscribe(data => {
      const updatedManufacturer = this.manufacturers.find(m => m.id === data.id);
      updatedManufacturer.id = data.id;
      updatedManufacturer.name = data.name;
      updatedManufacturer.description = data.description;
    });
  }

  onDelete(): void {
    this.manufacturersService.delete(this.selectedManufacturer).subscribe(data => {
      this.manufacturers = this.manufacturers.filter(m => m.id !== this.selectedManufacturer.id);
      this.selectedManufacturer = null;
    });
  }

  openCreationDialog(): void {
    const dialogRef = this.dialog.open(CreateManufacturerDialogComponent);

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
      setTimeout(() => {
        this.manufacturersService.getAll().subscribe(data => {
          this.manufacturers = data;
        });
      }, 1000);
    });
  }
}
