import { Component, OnInit } from '@angular/core';
import {ManufacturersService} from '../../manufacturers.service';
import {Manufacturer} from '../../model';

@Component({
  selector: 'crd-create-manufacturer-dialog',
  templateUrl: './create-manufacturer-dialog.component.html',
  styleUrls: ['./create-manufacturer-dialog.component.less']
})
export class CreateManufacturerDialogComponent implements OnInit {

  constructor(private manufacturerService: ManufacturersService) { }

  public manufacturer: Manufacturer = new Manufacturer();

  ngOnInit(): void {
  }

  createManufacturer(): void {
    this.manufacturerService.create(this.manufacturer).subscribe(
      data => {
        console.log('created = ', data);
      }, err => {
        console.log('cant create = ', err);
      }
    );
  }
}
