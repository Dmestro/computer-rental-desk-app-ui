import { Component, OnInit } from '@angular/core';
import {Manufacturer} from '../../../manufactirers/model';
import {ModelsService} from '../../../models/models.service';
import {ManufacturersService} from '../../../manufactirers/manufacturers.service';
import {EquipmentTypesService} from '../../../equipment-types/equipment-types.service';
import {EquipmentModel} from '../../../models/model';
import {Equipment} from '../../model';
import {EquipmentsService} from '../../equipments.service';

@Component({
  selector: 'crd-create-equipment-dialog',
  templateUrl: './create-equipment-dialog.component.html',
  styleUrls: ['./create-equipment-dialog.component.less']
})
export class CreateEquipmentDialogComponent implements OnInit {
  public selectedEquipment = new Equipment();

  constructor(
    private equipmentService: EquipmentsService,
    public modelsService: ModelsService,
  ) {
  }

  ngOnInit(): void {
  }

  create(): void {
    this.equipmentService.create(this.selectedEquipment);
  }


}
