import { Component, OnInit } from '@angular/core';
import {EquipmentsService} from '../../equipments.service';
import {EquipmentModel} from '../../../models/model';
import {CreateModelDialogComponent} from '../../../models/components/create-model-dialog/create-model-dialog.component';
import {Equipment} from '../../model';
import {ModelsService} from '../../../models/models.service';
import {CreateEquipmentDialogComponent} from '../create-equipment-dialog/create-equipment-dialog.component';
import {MatDialog} from '@angular/material/dialog';

@Component({
  selector: 'crd-equipments',
  templateUrl: './equipments.component.html',
  styleUrls: ['./equipments.component.less']
})
export class EquipmentsComponent implements OnInit {

  private selectedEquipment: Equipment;

  constructor(
    public equipmentsService: EquipmentsService,
    public modelsService: ModelsService,
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.equipmentsService.getAll();
    this.modelsService.getAll();
  }

  setSelectedItem(item: Equipment): void {
    this.selectedEquipment = JSON.parse(JSON.stringify(item));
    this.selectedEquipment.model = this.modelsService.equipmentModels.find(i => i.id === this.selectedEquipment.model.id);
  }

  onCancelEdit(): void {
    this.selectedEquipment = JSON.parse(JSON.stringify(
      this.equipmentsService.equipments.find(m => m.id === this.selectedEquipment.id)
    ));

    this.selectedEquipment.model = this.modelsService.equipmentModels.find(i => i.id === this.selectedEquipment.model.id);

  }

  onSave(): void {
    this.equipmentsService.save(this.selectedEquipment);
  }

  onDelete(): void {
    this.equipmentsService.delete(this.selectedEquipment).subscribe(data => {
      this.selectedEquipment = null;
    });
  }

  openCreationDialog(): void {


    const dialogRef = this.dialog.open(CreateEquipmentDialogComponent);

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }

}
