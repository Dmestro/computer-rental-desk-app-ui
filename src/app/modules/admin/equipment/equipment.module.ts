import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {EquipmentsComponent} from './components/equipments/equipments.component';
import {FormsModule} from '@angular/forms';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import {MatInputModule} from '@angular/material/input';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatNativeDateModule} from '@angular/material/core';
import {HttpClientModule} from '@angular/common/http';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatDialogModule} from '@angular/material/dialog';
import {MatSelectModule} from '@angular/material/select';
import { CreateEquipmentDialogComponent } from './components/create-equipment-dialog/create-equipment-dialog.component';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';



@NgModule({
  declarations: [EquipmentsComponent, CreateEquipmentDialogComponent],
  imports: [
    CommonModule,
    FormsModule,
    MatButtonModule,
    MatCardModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    HttpClientModule,
    MatSnackBarModule,
    MatExpansionModule,
    MatDialogModule,
    MatSelectModule,
    MatSlideToggleModule
  ],
  exports: [
    EquipmentsComponent
  ],
  entryComponents: [CreateEquipmentDialogComponent]
})
export class EquipmentModule { }
