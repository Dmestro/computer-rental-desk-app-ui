import {EquipmentModel} from '../models/model';
import {templateJitUrl} from '@angular/compiler';

export class Equipment {
  id: string;
  inventoryNumber: string;
  availability: boolean;
  location: string;

  releaseDate: string;

  model: EquipmentModel;
  rentPrice: number;
}
