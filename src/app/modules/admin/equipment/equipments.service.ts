import { Injectable } from '@angular/core';
import {EquipmentType} from '../equipment-types/model';
import {HttpClient} from '@angular/common/http';
import {Observable, Subject} from 'rxjs';
import {BACKEND_HOST} from '../../../constants';
import {map, take} from 'rxjs/operators';
import {Equipment} from './model';

@Injectable({
  providedIn: 'root'
})
export class EquipmentsService {

  public equipments: Equipment[] = [];

  private endpoint = '/equipment';

  constructor(private http: HttpClient) {
  }

  public getAll(): Observable<Equipment[]> {
    const res$ = this.http.get(BACKEND_HOST + this.endpoint);

    const s$ = new Subject<Equipment[]>();

    res$.pipe(take(1)).subscribe((data: Equipment[]) => {
      this.equipments = data;
      s$.next(data);
    });
    return s$.asObservable();
  }

  public save(equipment: Equipment): Observable<Equipment> {
    const res$ = this.http.put(BACKEND_HOST + this.endpoint + '/' + equipment.id, equipment);

    res$.pipe(take(1)).subscribe(
      (et: Equipment) => {
        const index: number = this.equipments.findIndex(i => i.id === et.id);
        if (index === -1) {
          this.equipments.push(et);
        } else {
          this.equipments[index] = et;
        }
      }
    );

    return res$.pipe(map(r => r as Equipment));

  }

  delete(equipment: Equipment): Observable<any> {
    const res$ = this.http.delete(BACKEND_HOST + this.endpoint + '/' + equipment.id);
    res$.pipe(take(1)).subscribe(
      () => {
        this.equipments = this.equipments.filter((et: Equipment) => et.id !== equipment.id);
      }
    );

    return res$;
  }

  create(equipment: Equipment): Observable<Equipment> {
    const res$ = this.http.post(BACKEND_HOST + this.endpoint, equipment);
    res$.subscribe((et: Equipment) => this.equipments.push(et));
    return res$.pipe(map(res => res as Equipment));
  }
}
