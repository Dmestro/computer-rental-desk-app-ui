export class User {
  id: string;
  name: string;
  lastName: string;
  email: string;
  phone: string;
  birthDate: any;
  address: string;

  role: string;

}
