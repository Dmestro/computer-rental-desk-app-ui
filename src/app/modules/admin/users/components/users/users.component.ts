import { Component, OnInit } from '@angular/core';
import {UsersService} from '../../users.service';
import {EquipmentModel} from '../../../models/model';
import {User} from '../../model';
import {Rent} from "../../../../user/rent/model";
import {Repair} from "../../../../user/user-repair/model";
import {RentService} from "../../../../user/rent/rent.service";
import {AuthService} from "../../../../auth/auth.service";
import {RepairService} from "../../../../user/user-repair/repair.service";
import {EquipmentsService} from "../../../equipment/equipments.service";

@Component({
  selector: 'crd-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.less']
})
export class UsersComponent implements OnInit {

  private selectedUser: User;
  public selectedRents: Rent[]
  public selectedRepairs: Repair[]

  constructor(public usersService: UsersService,
              public rentService: RentService,
              public authService: AuthService,
              public equipmentsService: EquipmentsService,
              public repairsService: RepairService,) { }

  ngOnInit(): void {
    this.usersService.getAll();
    this.repairsService.getAll();
    this.rentService.getAll();
    this.equipmentsService.getAll();
  }

  setSelectedItem(item: User): void {
    this.selectedUser = JSON.parse(JSON.stringify(item));
    this.selectedRents = this.rentService.rents.filter(r => r.user.id === this.selectedUser.id);
    this.selectedRents.forEach( r=> {
      r.equipment = this.equipmentsService.equipments.find(e => e.id === r.equipment.id);
    });

    this.selectedRepairs = this.repairsService.repairs.filter(r => r.user.id === this.selectedUser.id )
    this.selectedRepairs.forEach(r => {
      r.equipment = this.equipmentsService.equipments.find(e => e.id === r.equipment.id);
    })
  }

  updateRole(): void {
    this.usersService.updateUserRoleById(this.selectedUser.id, this.selectedUser.role).subscribe(data => {
      this.selectedUser.role = data.role;

      this.usersService.users.forEach(u => {
        if (u.id === data.id) {
          u.role = data.role;
        }
      });
    });
  }
}
