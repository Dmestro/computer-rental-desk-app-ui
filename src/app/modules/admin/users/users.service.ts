import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, Subject} from 'rxjs';
import {Equipment} from '../equipment/model';
import {BACKEND_HOST} from '../../../constants';
import {map, take} from 'rxjs/operators';
import {User} from './model';
import {Rent} from '../../user/rent/model';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  private endpoint = '/user';
  public users: User[] = [];

  constructor(private http: HttpClient) { }

  public getAll(): Observable<User[]> {
    const res$ = this.http.get(BACKEND_HOST + this.endpoint);

    const s$ = new Subject<User[]>();

    res$.pipe(take(1)).subscribe((data: User[]) => {
      this.users = data;
      s$.next(data);
    });
    return s$.asObservable();
  }

  public save(equipment: Equipment): Observable<User> {
    const res$ = this.http.put(BACKEND_HOST + this.endpoint + '/' + equipment.id, equipment);

    res$.pipe(take(1)).subscribe(
      (et: User) => {
        const index: number = this.users.findIndex(i => i.id === et.id);
        if (index === -1) {
          this.users.push(et);
        } else {
          this.users[index] = et;
        }
      }
    );

    return res$.pipe(map(r => r as User));

  }

  delete(equipment: Equipment): Observable<any> {
    const res$ = this.http.delete(BACKEND_HOST + this.endpoint + '/' + equipment.id);
    res$.pipe(take(1)).subscribe(
      () => {
        this.users = this.users.filter((et: User) => et.id !== equipment.id);
      }
    );

    return res$;
  }

  create(equipment: User): Observable<User> {
    const res$ = this.http.post(BACKEND_HOST + this.endpoint, equipment);
    res$.subscribe((et: User) => this.users.push(et));
    return res$.pipe(map(res => res as User));
  }

  updateUserRoleById(userId: string, role: string): Observable<User> {
    return this.http.post<User>(BACKEND_HOST + this.endpoint + '/update-role/' + userId, {
      role
    });
  }

}
