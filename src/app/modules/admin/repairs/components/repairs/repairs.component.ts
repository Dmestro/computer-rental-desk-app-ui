import { Component, OnInit } from '@angular/core';
import {RepairService} from '../../../../user/user-repair/repair.service';
import {Repair} from '../../../../user/user-repair/model';
import {combineLatest} from 'rxjs';
import {EquipmentsService} from '../../../equipment/equipments.service';
import {RentService} from '../../../../user/rent/rent.service';
import {UsersService} from '../../../users/users.service';
import {AuthService} from '../../../../auth/auth.service';
import {MatDialog} from '@angular/material/dialog';
import {GetRepairDialogComponent} from '../../../../user/user-main-page/get-repair-dialog/get-repair-dialog.component';
import {SelectEngineerDialogComponent} from '../select-engineer-dialog/select-engineer-dialog.component';

@Component({
  selector: 'crd-repairs',
  templateUrl: './repairs.component.html',
  styleUrls: ['./repairs.component.less']
})
export class RepairsComponent implements OnInit {
  public repairs: Repair[];
  private selectedRepair: Repair;

  constructor(
    public equipmentsService: EquipmentsService,
    public rentService: RentService,
    public usersService: UsersService,
    public authService: AuthService,
    public repairsService: RepairService,
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
    combineLatest(
      this.equipmentsService.getAll(),
      this.usersService.getAll(),
      this.repairsService.getAll()
    ).subscribe(([equipments, users, repairs]) => {
      if (equipments && users && repairs) {
        this.updateData(equipments, users, repairs);
      }
    });
  }

  public updateData(equipments, users, repairs: Repair[]): void {
    this.repairs = repairs.filter(r => !r.serviceEngineer && r.status === 'new');

    this.repairs.forEach(r => {
      r.equipment = equipments.find(e => e.id === r.equipment.id);
    });
  }

  setSelectedItem(et: Repair): void {
    this.selectedRepair = et;
  }

  openSelectEngeneerDialog(): void {
    const dialogRef = this.dialog.open(SelectEngineerDialogComponent, {
      data: {
        repair: this.selectedRepair
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      combineLatest(
        this.equipmentsService.getAll(),
        this.usersService.getAll(),
        this.repairsService.getAll()
      ).subscribe(([equipments, users, repairs]) => {
        if (equipments && users && repairs) {
          this.updateData(equipments, users, repairs);
        }
      });
    });
  }
}
