import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {UsersService} from '../../../users/users.service';
import {User} from '../../../users/model';
import {Repair} from '../../../../user/user-repair/model';
import {RepairService} from '../../../../user/user-repair/repair.service';

class DialogData {
}

@Component({
  selector: 'crd-select-engineer-dialog',
  templateUrl: './select-engineer-dialog.component.html',
  styleUrls: ['./select-engineer-dialog.component.less']
})
export class SelectEngineerDialogComponent implements OnInit {
  public engineers: User[];
  selectedEngineer: User;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private usersService: UsersService,
    private repairsService: RepairService
  ) { }

  ngOnInit(): void {
    this.usersService.getAll().subscribe(users => {
      this.engineers = users.filter(u => u.role === 'engineer');
    });
  }

  selectEngineer(): void {
    console.log("SE = ", this.selectedEngineer, this.data);
    const repair: Repair = (this.data as any).repair;
    repair.serviceEngineer = this.selectedEngineer;

    this.repairsService.save(repair);
  }
}
