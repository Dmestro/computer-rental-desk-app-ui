import { Component, OnInit } from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {EquipmentTypesService} from '../../equipment-types.service';
import {EquipmentType} from '../../model';
import {CreateEquipmentTypeDialogComponent} from '../create-equipment-type-dialog/create-equipment-type-dialog.component';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'crd-equipment-types',
  templateUrl: './equipment-types.component.html',
  styleUrls: ['./equipment-types.component.less']
})
export class EquipmentTypesComponent implements OnInit {

  private selectedEquipmentType: EquipmentType;

  constructor(
    public equipmentTypesService: EquipmentTypesService,
    private dialog: MatDialog,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit(): void {
    this.equipmentTypesService.getAll();
  }

  setSelectedManufacturer(equipmentType: any): void {
    this.selectedEquipmentType = JSON.parse(JSON.stringify(equipmentType));
  }

  onCancelEdit(): void {
    this.selectedEquipmentType = JSON.parse(JSON.stringify(
      this.equipmentTypesService.equipmentTypes.find(m => m.id === this.selectedEquipmentType.id)
    ));
  }

  onSave(): void {
    this.equipmentTypesService.save(this.selectedEquipmentType);
  }

  onDelete(): void {
    this.equipmentTypesService.delete(this.selectedEquipmentType).subscribe(data => {
      this.selectedEquipmentType = null;
    }, () => {
      this.snackBar.open(
        'Вид техники ' + this.selectedEquipmentType.name + ' не может быть удален, так как он используется в системе',
        null,
        {duration: 2000});
    });
  }

  openCreationDialog(): void {

    const dialogRef = this.dialog.open(CreateEquipmentTypeDialogComponent);

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }
}
