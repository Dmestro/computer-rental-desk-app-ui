import { Component, OnInit } from '@angular/core';
import {ManufacturersService} from '../../../manufactirers/manufacturers.service';
import {Manufacturer} from '../../../manufactirers/model';
import {EquipmentTypesService} from '../../equipment-types.service';
import {EquipmentType} from '../../model';

@Component({
  selector: 'crd-create-equipment-type-dialog',
  templateUrl: './create-equipment-type-dialog.component.html',
  styleUrls: ['./create-equipment-type-dialog.component.less']
})
export class CreateEquipmentTypeDialogComponent implements OnInit {

  constructor(private equipmentTypesService: EquipmentTypesService) { }

  public equipmentType: EquipmentType  = new EquipmentType();

  ngOnInit(): void {
  }

  create(): void {
    this.equipmentTypesService.create(this.equipmentType);
  }

}
