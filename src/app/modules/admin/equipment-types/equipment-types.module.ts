import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {EquipmentTypesComponent} from './components/equipment-types/equipment-types.component';
import {SharedModule} from '../../shared/shared.module';
import {FormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import {EquipmentTypesService} from './equipment-types.service';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import {MatInputModule} from '@angular/material/input';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatNativeDateModule} from '@angular/material/core';
import {HttpClientModule} from '@angular/common/http';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatDialogModule} from '@angular/material/dialog';
import { CreateEquipmentTypeDialogComponent } from './components/create-equipment-type-dialog/create-equipment-type-dialog.component';


@NgModule({
  declarations: [EquipmentTypesComponent, CreateEquipmentTypeDialogComponent],
  imports: [
    CommonModule,
    FormsModule,
    MatButtonModule,
    MatCardModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    HttpClientModule,
    MatSnackBarModule,
    MatExpansionModule,
    MatDialogModule
  ],
  exports: [EquipmentTypesComponent],
  entryComponents: [
    CreateEquipmentTypeDialogComponent
  ]
})
export class EquipmentTypesModule {
}
