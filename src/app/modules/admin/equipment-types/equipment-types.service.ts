import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {BACKEND_HOST} from '../../../constants';
import {EquipmentType} from './model';
import {map, take, tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class EquipmentTypesService {

  public equipmentTypes: EquipmentType[] = [];

  private endpoint = '/equipment-types';

  constructor(private http: HttpClient) {
  }

  public getAll(): Observable<EquipmentType[]> {
    const res$ = this.http.get(BACKEND_HOST + this.endpoint);
    res$.pipe(take(1)).subscribe((data: EquipmentType[]) => this.equipmentTypes = data);
    return res$.pipe(map(r => r as EquipmentType[]));
  }

  public save(equipmentType: EquipmentType): Observable<EquipmentType> {
    const res$ = this.http.put(BACKEND_HOST + this.endpoint + '/' + equipmentType.id, equipmentType);

    res$.pipe(take(1)).subscribe(
      (et: EquipmentType) => {
        const index: number = this.equipmentTypes.findIndex(i => i.id === et.id);
        if (index === -1) {
          this.equipmentTypes.push(et);
        } else {
          this.equipmentTypes[index] = et;
        }
      }
    );

    return res$.pipe(map(r => r as EquipmentType));

  }

  delete(equipmentType: EquipmentType): Observable<any> {
    const res$ = this.http.delete(BACKEND_HOST + this.endpoint + '/' + equipmentType.id);
    res$.pipe(take(1)).subscribe(
      () => {
        this.equipmentTypes = this.equipmentTypes.filter((et: EquipmentType) => et.id !== equipmentType.id);
      }
    );

    return res$;
  }

  create(equipmentType: EquipmentType): Observable<EquipmentType> {
    const res$ = this.http.post(BACKEND_HOST + this.endpoint, equipmentType);
    res$.subscribe((et: EquipmentType) => this.equipmentTypes.push(et));
    return res$.pipe(map(res => res as EquipmentType));
  }
}
