import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {EquipmentType} from '../equipment-types/model';
import {BACKEND_HOST} from '../../../constants';
import {map, take} from 'rxjs/operators';
import {EquipmentModel} from './model';

@Injectable({
  providedIn: 'root'
})
export class ModelsService {

  public equipmentModels: EquipmentModel[] = [];

  private endpoint = '/equipment-models';

  constructor(private http: HttpClient) {
  }

  public getAll(): Observable<EquipmentModel[]> {
    const res$ = this.http.get(BACKEND_HOST + this.endpoint);
    res$.pipe(take(1)).subscribe((data: EquipmentModel[]) => this.equipmentModels = data);
    return res$.pipe(map(r => r as EquipmentModel[]));
  }

  public save(equipmentModel: EquipmentModel): Observable<EquipmentModel> {
    const res$ = this.http.put(BACKEND_HOST + this.endpoint + '/' + equipmentModel.id, equipmentModel);

    res$.pipe(take(1)).subscribe(
      (et: EquipmentModel) => {
        const index: number = this.equipmentModels.findIndex(i => i.id === et.id);
        if (index === -1) {
          this.equipmentModels.push(et);
        } else {
          this.equipmentModels[index] = et;
        }
      }
    );

    return res$.pipe(map(r => r as EquipmentModel));

  }

  delete(equipmentModel: EquipmentModel): Observable<any> {
    const res$ = this.http.delete(BACKEND_HOST + this.endpoint + '/' + equipmentModel.id);
    res$.pipe(take(1)).subscribe(
      () => {
        this.equipmentModels = this.equipmentModels.filter((et: EquipmentModel) => et.id !== equipmentModel.id);
      }
    );

    return res$;
  }

  create(equipmentModel: EquipmentModel): Observable<EquipmentModel> {
    const res$ = this.http.post(BACKEND_HOST + this.endpoint, equipmentModel);
    res$.subscribe((et: EquipmentModel) => this.equipmentModels.push(et));
    return res$.pipe(map(res => res as EquipmentModel));
  }
}
