import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ModelsComponent} from './components/models/models.component';
import {MatExpansionModule} from '@angular/material/expansion';
import {FormsModule} from '@angular/forms';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import {MatInputModule} from '@angular/material/input';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatNativeDateModule} from '@angular/material/core';
import {HttpClientModule} from '@angular/common/http';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatDialogModule} from '@angular/material/dialog';
import {MatSelectModule} from '@angular/material/select';
import { CreateModelDialogComponent } from './components/create-model-dialog/create-model-dialog.component';



@NgModule({
  declarations: [
    ModelsComponent,
    CreateModelDialogComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    MatButtonModule,
    MatCardModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    HttpClientModule,
    MatSnackBarModule,
    MatExpansionModule,
    MatDialogModule,
    MatSelectModule,
  ],
  exports: [
    ModelsComponent
  ],
  entryComponents: [
    CreateModelDialogComponent
  ]
})
export class ModelsModule { }
