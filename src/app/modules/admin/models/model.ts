import {Manufacturer} from '../manufactirers/model';
import {EquipmentType} from '../equipment-types/model';

export class EquipmentModel {
  id: string;
  name: string;
  parameters: {};
  manufacturer: Manufacturer;
  type: EquipmentType;
}
