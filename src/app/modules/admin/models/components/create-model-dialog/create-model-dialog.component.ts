import {Component, OnInit} from '@angular/core';
import {EquipmentTypesService} from '../../../equipment-types/equipment-types.service';
import {EquipmentType} from '../../../equipment-types/model';
import {EquipmentModel} from '../../model';
import {ModelsService} from '../../models.service';
import {Manufacturer} from '../../../manufactirers/model';
import {ManufacturersService} from '../../../manufactirers/manufacturers.service';

@Component({
  selector: 'crd-create-model-dialog',
  templateUrl: './create-model-dialog.component.html',
  styleUrls: ['./create-model-dialog.component.less']
})
export class CreateModelDialogComponent implements OnInit {

  public manufacturers: Manufacturer[] = [];

  constructor(
    private equipmentTypesService: ModelsService,
    public manufacturersService: ManufacturersService,
    public typesService: EquipmentTypesService,
  ) {
  }

  public equipmentModel: EquipmentModel = new EquipmentModel();

  ngOnInit(): void {
    this.manufacturersService.getAll().subscribe(data => {
      this.manufacturers = data;
    });
  }

  create(): void {
    this.equipmentTypesService.create(this.equipmentModel);
  }

}
