import { Component, OnInit } from '@angular/core';
import {EquipmentType} from '../../../equipment-types/model';
import {EquipmentTypesService} from '../../../equipment-types/equipment-types.service';
import {MatDialog} from '@angular/material/dialog';
import {CreateEquipmentTypeDialogComponent} from '../../../equipment-types/components/create-equipment-type-dialog/create-equipment-type-dialog.component';
import {EquipmentModel} from '../../model';
import {ModelsService} from '../../models.service';
import {ManufacturersService} from '../../../manufactirers/manufacturers.service';
import {Manufacturer} from '../../../manufactirers/model';
import {CreateModelDialogComponent} from '../create-model-dialog/create-model-dialog.component';

@Component({
  selector: 'crd-models',
  templateUrl: './models.component.html',
  styleUrls: ['./models.component.less']
})
export class ModelsComponent implements OnInit {

  private selectedEquipmentModel: EquipmentModel;

  public manufacturers: Manufacturer[] = [];

  constructor(
    public equipmentModelsService: ModelsService,
    public manufacturersService: ManufacturersService,
    public typesService: EquipmentTypesService,
    private dialog: MatDialog
  ) {


  }

  ngOnInit(): void {
    this.equipmentModelsService.getAll();
    this.manufacturersService.getAll().subscribe(data => {
      this.manufacturers = data;
    });
    this.typesService.getAll();
  }

  setSelectedItem(item: EquipmentModel): void {
    this.selectedEquipmentModel = JSON.parse(JSON.stringify(item));
    this.selectedEquipmentModel.manufacturer = this.manufacturers.find(i => i.id === this.selectedEquipmentModel.manufacturer.id);
    this.selectedEquipmentModel.type = this.typesService.equipmentTypes.find(i => i.id === this.selectedEquipmentModel.type.id);
  }

  onCancelEdit(): void {
    this.selectedEquipmentModel = JSON.parse(JSON.stringify(
      this.equipmentModelsService.equipmentModels.find(m => m.id === this.selectedEquipmentModel.id)
    ));

    this.selectedEquipmentModel.manufacturer = this.manufacturers.find(i => i.id === this.selectedEquipmentModel.manufacturer.id);
    this.selectedEquipmentModel.type = this.typesService.equipmentTypes.find(i => i.id === this.selectedEquipmentModel.type.id);
  }

  onSave(): void {
    this.equipmentModelsService.save(this.selectedEquipmentModel);
  }

  onDelete(): void {
    this.equipmentModelsService.delete(this.selectedEquipmentModel).subscribe(data => {
      this.selectedEquipmentModel = null;
    });
  }

  openCreationDialog(): void {


    const dialogRef = this.dialog.open(CreateModelDialogComponent);

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }

}
