import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {RegistrationParams} from '../../model';
import {HttpClient} from '@angular/common/http';
import {AuthService} from '../../auth.service';
import {MatSnackBar} from '@angular/material/snack-bar';



@Component({
  selector: 'crd-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.less']
})
export class RegistrationComponent implements OnInit {
  public registrationParams: RegistrationParams;

  @Output() onSuccess: EventEmitter<any> = new EventEmitter<any>();

  constructor(
    private auth: AuthService,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit(): void {
    this.registrationParams = new RegistrationParams();
  }

  public onSubmit(): void {
    this.auth.register(this.registrationParams).subscribe(data => {
      this.snackBar.open('Поздравляем, Вы успешно зарегистрированы!' , ' ', {duration: 2000});
      this.onSuccess.emit();
    }, ({error}) => {
      this.snackBar.open(error.message || 'Ошибка. Пожалуйста, проверьте корректность введенных данных');
    });
  }

}
