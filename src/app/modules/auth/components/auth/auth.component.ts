import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'crd-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.less']
})
export class AuthComponent implements OnInit {

  public mode: 'login' | 'registration' = 'login';

  constructor() {
  }

  public toggleMode(): void {
    if (this.mode === 'login') {
      this.mode = 'registration';
    } else {
      this.mode = 'login';
    }
  }

  ngOnInit(): void {
  }

}
