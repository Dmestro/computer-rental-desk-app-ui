import { Component, OnInit } from '@angular/core';
import {LoginParams} from '../../model';
import {AuthService} from '../../auth.service';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'crd-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less']
})
export class LoginComponent implements OnInit {

  public loginParams: LoginParams;

  constructor(
    private auth: AuthService,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit(): void {
    this.loginParams = new LoginParams();
  }

  public onSubmit(): void {
    this.auth.login(this.loginParams).subscribe(_ => {}, ({error}) => {
      this.snackBar.open(
        error.message,
        'ОК', {
          duration: 2000
        }
      );
    });
  }

}
