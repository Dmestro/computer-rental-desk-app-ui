export class RegistrationParams {
  email: string;
  password: string;
  name: string;
  lastName: string;
  phone: string;
  address: string;
  birthDate: Date;
}

export class LoginParams {
  email: string;
  password: string;
}
