import { Injectable } from '@angular/core';
import {LoginParams, RegistrationParams} from './model';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';
import {LocalStorageService} from '../shared/local-storage.service';
import {BACKEND_HOST} from '../../constants';
import {Router} from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private readonly LOCAL_USER_KEY = 'authData';

  private userData: {token: string, role: string, id: string};

  constructor(
    private http: HttpClient,
    private localStorage: LocalStorageService,
    private router: Router
    ) {
    this.userData = this.localStorage.getItem(this.LOCAL_USER_KEY);
  }

  public navigate() {
    console.log("UD = ", this.userData);
    switch (this.userData.role) {
      case "admin": {
        this.router.navigate(['/users']);
        break;
      }
      case "engineer": {
        this.router.navigate(['/engineer-new-tickets']);
        break;
      }
      case "user": {
        this.router.navigate(['/user-main-page']);
        break;
      }
      default: {

      }

    }
  }

  public register(userParams: RegistrationParams): Observable<any> {
    return this.http.post(BACKEND_HOST + '/auth/register', userParams);
  }

  public login(loginParams: LoginParams): Observable<any> {
    return this.http.post(BACKEND_HOST + '/auth/login', loginParams).pipe(
      tap(userData => {
        this.userData = userData;
        this.localStorage.setItem(this.LOCAL_USER_KEY, userData);
        this.navigate();
      })
    );
  }

  public isUserLogined(): boolean {
    return !!this.userData;
  }

  public logout(): void {
    this.userData = null;
    this.localStorage.setItem(this.LOCAL_USER_KEY, this.userData);
  }

  public getUserParams(): any {
    return this.userData;
  }

}
