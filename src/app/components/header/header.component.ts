import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../modules/auth/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'crd-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.less']
})
export class HeaderComponent implements OnInit {

  constructor(
    public auth: AuthService,
    public router: Router
  ) { }

  ngOnInit(): void {
  }

}
