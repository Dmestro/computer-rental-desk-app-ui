import { Component } from '@angular/core';
import {AuthService} from './modules/auth/auth.service';

@Component({
  selector: 'crd-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent {

  constructor(
    public auth: AuthService
  ) {
  }

}
