import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {SharedModule} from './modules/shared/shared.module';
import { MatButtonModule } from '@angular/material/button';
import { HeaderComponent } from './components/header/header.component';
import {CommonModule} from '@angular/common';
import {ManufactirersModule} from './modules/admin/manufactirers/manufactirers.module';
import {EquipmentTypesModule} from './modules/admin/equipment-types/equipment-types.module';
import {AuthModule} from './modules/auth/auth.module';
import { UserMainPageComponent } from './modules/user/user-main-page/user-main-page.component';
import { RentComponent } from './modules/user/rent/rent.component';
import { UserRepairComponent } from './modules/user/user-repair/user-repair.component';
import {FormsModule} from '@angular/forms';
import {MatCardModule} from '@angular/material/card';
import {MatInputModule} from '@angular/material/input';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatNativeDateModule} from '@angular/material/core';
import {HttpClientModule} from '@angular/common/http';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatDialogModule} from '@angular/material/dialog';
import {MatSelectModule} from '@angular/material/select';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import { GetRepairDialogComponent } from './modules/user/user-main-page/get-repair-dialog/get-repair-dialog.component';
import {MatToolbarModule} from '@angular/material/toolbar';
import { NewTicketsComponent } from './modules/repair-engineer/new-tickets/new-tickets.component';
import { InWorkTicketsComponent } from './modules/repair-engineer/in-work-tickets/in-work-tickets.component';
import { ClosedTicketsComponent } from './modules/repair-engineer/closed-tickets/closed-tickets.component';
import {MatTabsModule} from "@angular/material/tabs";


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    UserMainPageComponent,
    RentComponent,
    UserRepairComponent,
    GetRepairDialogComponent,
    NewTicketsComponent,
    InWorkTicketsComponent,
    ClosedTicketsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    AuthModule,
    SharedModule,
    EquipmentTypesModule,
    MatButtonModule,
    FormsModule,
    MatCardModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    HttpClientModule,
    MatSnackBarModule,
    MatExpansionModule,
    MatDialogModule,
    MatSelectModule,
    MatSlideToggleModule,
    MatToolbarModule,
    MatTabsModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [
    GetRepairDialogComponent
  ]
})
export class AppModule { }
